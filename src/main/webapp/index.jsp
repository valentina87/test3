<html>
<body>
<h2>Tomcat 7 Clickstack Demo</h2>
<ul>
    <li><a href="MyServlet">Annotated Servlet demo: @WebServlet and @Resource to lookup the datasource</a></li>
    <li><a href="datasource.jsp">JNDI DataSource demo</a></li>
    <li><a href="datasource-jstl.jsp">JNDI DataSource demo with JSTL</a></li>
</ul>
</body>
</html>
